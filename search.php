<?php

$con = require('connectdb.php');


$from = isset($_POST['from']) ? $_POST['from'] :  date('Y-m-d H:i:s');
$to = isset($_POST['to'])? $_POST['to'] : date('Y-m-d H:i:s');

$lowestLowResultSQL = "SELECT MIN(low) as value, server_time FROM ticker WHERE server_time BETWEEN '{$from}' AND '{$to}' LIMIT 0,1";
$statement = $conn->query($lowestLowResultSQL);
$lowestLowResult = $statement->fetch(PDO::FETCH_ASSOC);

$lowestMarketPriceResultSQL = "SELECT MIN(sell) as value, server_time FROM ticker WHERE server_time BETWEEN '{$from}' AND '{$to}' LIMIT 0,1";
$statement = $conn->query($lowestMarketPriceResultSQL);
$lowestMarketPriceResult = $statement->fetch(PDO::FETCH_ASSOC);

$highestMarketPriceResultSQL = "SELECT MAX(sell) as value, server_time FROM ticker WHERE server_time BETWEEN '{$from}' AND '{$to}' LIMIT 0,1";
$statement = $conn->query($highestMarketPriceResultSQL);
$highestMarketPriceResult = $statement->fetch(PDO::FETCH_ASSOC);

$highestHighResultSQL = "SELECT MAX(high) as value, server_time FROM ticker WHERE server_time BETWEEN '{$from}' AND '{$to}' LIMIT 0,1";
$statement = $conn->query($highestHighResultSQL);
$highestHighResult = $statement->fetch(PDO::FETCH_ASSOC);

$currentTimeSQL = "SELECT MAX(server_time) current FROM ticker";
$statement = $conn->query($currentTimeSQL);
$currentTimeResult = $statement->fetch(PDO::FETCH_ASSOC);
$currentTime = $currentTimeResult['current'];

$currentSQL = "SELECT high, low, sell FROM ticker WHERE server_time = '{$currentTime}'";
$statement = $conn->query($currentSQL);
$current = $statement->fetch(PDO::FETCH_ASSOC);	


$data = array(
		'lowestLow' => 
			array(
				'value' => round($lowestLowResult['value']), 
				'date' =>date('dS M Y H:i',strtotime($lowestLowResult['server_time'])
			)),
		'lowestMarketPrice' => 
			array(
				'value' => round($lowestMarketPriceResult['value']), 
				'date' =>date('dS M Y H:i',strtotime($lowestMarketPriceResult['server_time'])
			)),
		'highestMarketPrice' => 
			array(
				'value' => round($highestMarketPriceResult['value']), 
				'date' =>date('dS M Y H:i',strtotime($highestMarketPriceResult['server_time'])
			)),
		'highestHigh' => 
			array(
				'value' => round($highestHighResult['value']), 
				'date' => date('dS M Y H:i',strtotime($highestHighResult['server_time'])	
			)),
		'currentHigh' =>round($current['high']),
		'currentMarketPrice' => round($current['sell']),
		'currentLow' => round($current['low']),
	);

header('Content-Type: application/json');
echo json_encode($data);