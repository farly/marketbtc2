<?php

try {
    $conn = new PDO('mysql:host=localhost;dbname=farly_marketbtc', 'farly', 'D1sc0v3ry');
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo 'ERROR: ' . $e->getMessage();
}

return $conn;

