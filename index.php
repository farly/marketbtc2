<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>MarketBTC</title>
        <link rel="stylesheet" href="css/jquery.datetimepicker.css">
        <link rel="stylesheet" href="css/bootstrap.css" />
    </head>
    <body>
        <div class="container">
            <div id='main_content'>

				<div class="row">
					<div class="span6 pagination-centered">
						<label for="from_date">From:</label>
						<input name="from_date" type="text" />			
					</div>

					<div class="span6 pagination-centered">
						<label for="to_date">To:</label>
						<input name="to_date" type="text" />		
					</div>
				</div>

				<div class="row">
					<div class="span12 pagination-centered">
						<a href="#" id="search" >Search</a>	
					</div>
				</div>

				<div class="row" id="results">
				</div>
			</div>
			<script data-main="/js/main.js" src="/js/lib/require.js"></script></span>
        </div>
    </body>
</html>

