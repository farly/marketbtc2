<?php

$conn = require('connectdb.php');


$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, "https://btc-e.com/api/2/btc_usd/ticker");
$result = json_decode(curl_exec($ch),true);

$result = $result['ticker'];

$result['average'] = $result['avg'];
		
/**
* can be derived though 
*/
$result['diff_high_low'] = $result['high'] - $result['low'];
$result['diff_high_last'] = $result['high'] - $result['last'];
$result['diff_low_last'] = $result['last'] - $result['low'];
unset($result['avg']);

try {
	$update = date('Y-m-d H:i:s', $result['updated']);
	$server_time = date('Y-m-d H:i:s', $result['server_time']);

	$sql = "INSERT INTO ticker(high,low,average,vol,vol_cur,last,buy,sell,updated,server_time,diff_high_low,diff_high_last,diff_low_last) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
	$statement = $conn->prepare($sql);
	$statement->bindParam(1,$result['high']);
	$statement->bindParam(2,$result['low']);
	$statement->bindParam(3,$result['low']);
	$statement->bindParam(4,$result['vol']);
	$statement->bindParam(5,$result['vol_cur']);
	$statement->bindParam(6,$result['last']);
	$statement->bindParam(7,$result['buy']);
	$statement->bindParam(8,$result['sell']);
	$statement->bindParam(9,$update);
	$statement->bindParam(10,$server_time);
	$statement->bindParam(11,$result['diff_high_low']);
	$statement->bindParam(12,$result['diff_high_last']);
	$statement->bindParam(13,$result['diff_low_last']);

	$statement->execute();	
} catch(Exception $e) {
	echo $e->getMessage();
}
