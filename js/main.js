/**
 * @author Farly Taboada <farly.taboada@gmail.com>
 */

/**
 * paths of library that will be loaded "dynamically" with its alias corresponding aliases
 * shim "injects dependency" | assigns module name
 */
require.config({
	paths: {
		text : 'lib/text',
		domReady: 'lib/domReady',
		jquery : 'http://code.jquery.com/jquery-2.0.3.min',
		datetimepicker: 'lib/jquery.datetimepicker',
		underscore: 'http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min'
	},
	shim: {
		/**
		 * Format
		 *
		 * alias configured in path : {
		 *   deps: [dependency: use the alias here]
		 *   exports: "name you want to use in calling the module: ex. $ for jQuery"
		 * }
		 *
		 */

		 datetimepicker:  {
		 	deps: ['jquery']
		 },

		 underscore: {
		 	exports: '_'
		 }

	}
});

require(['domReady','jquery','results','datetimepicker'],function(domReady,$,results){
	domReady(function(){

		$('input[name=from_date]').datetimepicker({
			format:'Y-m-d H:i:s',
			onShow:function( ct ){
		   		this.setOptions({
		    		maxDateTime:$('input[name=to_date]').val()?$('input[name=to_date]').val():false
		   		});
			}
		});

		$('input[name=to_date]').datetimepicker({
			format:'Y-m-d H:i:s',
			onShow:function( ct ){
				this.setOptions({
		    		maxDateTime:$('input[name=from_date]').val()?$('input[name=from_date]').val():false
			 	});
			}
		});

		$('#search').unbind().on('click',function(event){
			event.preventDefault();

			var me = $(this);
			me.hide();
			$('#results').empty().append('<span>Searching</span>');
			$.post('search.php',{'from': $('input[name=from_date]').val(), 'to': $('input[name=to_date]').val()}, function(data){

				results.show(data,$('#results'));
				me.show();
			});
		})
	});
});