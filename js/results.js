define(['text!result.tpl','underscore'], function(template, _){
	var template = _.template(template);

	var fxn = {
		show: function(data,parent) {
			parent.empty().html(template({results:data}));
		}
	}

	return fxn;
});