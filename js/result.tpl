<% if (results.lowestLow.value == 0) { %>
    	<div class="span12 pagination-centered">
    		<span>
    			No Data for the selected dates
    		</span>
    	<div>
<% } else {%>

<div class="span4">
	<div class="span12">
		<span><b>Lowest Low:</b></span>
	</div>
	<div class="span12">
		<span><b>Lowest Markert Price:</b></span>
	</div>
	<div class="span12">
		<span><b>Highest Market Price:</b></span>
	</div>
	<div class="span12">
		<span><b>Highest High:</b></span>
	</div>
	<div class="span12">
		<span><b>Current High:</b></span>
	</div>
	<div class="span12">
		<span><b>Current Markert Price:</b></span>
	</div>
	<div class="span12">
		<span><b>Current Low</b></span>
	</div>
</div>
			

<div class="span4">
	<div class="span12"><%= results.lowestLow.value  %></div>
	<div class="span12"><%= results.lowestMarketPrice.value  %></div>
	<div class="span12"><%= results.highestMarketPrice.value  %></div>
	<div class="span12"><%= results.highestHigh.value  %></div>
	<div class="span12"><%= results.currentHigh  %></div>
	<div class="span12"><%= results.currentMarketPrice  %></div>
	<div class="span12"><%= results.currentLow  %></div>
</div>

<div class="span4">
	<div class="span12"><%= results.lowestLow.date %></div>
	<div class="span12"><%= results.lowestMarketPrice.date  %></div>
	<div class="span12"><%= results.highestMarketPrice.date  %></div>
	<div class="span12"><%= results.highestHigh.date  %></div>
	<div class="span12"><span>-</span></div>
	<div class="span12"><%= results.currentHigh - results.currentMarketPrice%> (difference)</div>
	<div class="span12"><%= results.currentMarketPrice - results.currentLow%> (difference)</div>
</div>

<% } %>